<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Service;
class ServiceController extends Controller
{
    public function index(){

        // $services = [
        //     'New Service 1',
        //     'New Service 2',
        //     'New Service 3',
        //     'New Service 4'
        // ];

        $services = Service::all();
        
        return view('service.index', compact('services'));
    }

    public function store()
    {
        $data = request()->validate([
            'name' => 'required|min:3'
        ]);

        Service::create($data);
        // $service = new Service();;
        // $service->name = request('name');
        // $service->save();
        
        return redirect()->back();
    }
}
