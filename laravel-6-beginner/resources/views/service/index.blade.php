@extends('app')

@section('title', 'Services Page')


@section('content')
<h1>Services</h1>
<form action="/service" method="POST">
    @csrf
    <input type="text" name="name" autocomplete="off">

    <button>Add Service</button>
</form>

@error('name') {{ $message }} @enderror

<ul>
    @forelse($services as $service)
    <li>{{ $service->name }}</li>
    @empty
    <li>No Services Available....</li>
    @endforelse
</ul>
@endsection