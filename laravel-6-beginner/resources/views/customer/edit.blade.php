<h1>Edit Customer Details</h1>
<a href="/customers/{{ $customer->id }}">Back</a>
<form action="/customers/{{ $customer->id }}" method="POST">
    @method('PUT')

    @include('customer.form')

    <button>Update</button>
</form>