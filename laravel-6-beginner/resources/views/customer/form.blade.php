@csrf
<div>
    <label for="name">Name: </label>
    <input type="text" name="name" placeholder="Name..." autocomplete="off"
        value="{{ old('name') ?? $customer->name }}">
    @error('name') <p> {{ $message }}</p> @enderror
</div>
<div>
    <label for="email">Email: </label>
    <input type="text" name="email" placeholder="Email@email.com" autocomplete="off"
        value="{{ old('email') ?? $customer->email }}">
    @error('email') <p> {{ $message }}</p> @enderror
</div>